module.exports = {
  parserPreset: "conventional-changelog-conventionalcommits",
  rules: {
    "body-leading-blank": [1, "always"],
    "body-max-line-length": [2, "always", 100],
    "footer-leading-blank": [1, "always"],
    "footer-max-line-length": [2, "always", 100],
    "header-max-length": [2, "always", 100],
    "subject-case": [
      2,
      "never",
      ["sentence-case", "start-case", "pascal-case", "upper-case"],
    ],
    "subject-empty": [2, "never"],
    "subject-full-stop": [2, "never", "."],
    "type-case": [2, "always", "lower-case"],
    "type-empty": [2, "never"],
    "type-enum": [
      2,
      "always",
      [
        "build",
        "chore",
        "ci",
        "docs",
        "feat",
        "fix",
        "perf",
        "refactor",
        "revert",
        "style",
        "test",
        "release_ops",
        "release_test",
      ],
    ],
  },
  prompt: {
    questions: {
      type: {
        description: "커밋 형식을 골라 주세요.",
        enum: {
          feat: {
            description: "새로운 기능 추가",
            title: "Features",
            emoji: "✨",
          },
          fix: {
            description: "버그 수정",
            title: "Bug Fixes",
            emoji: "🐛",
          },
          docs: {
            description: "문서 작성 또는 수정",
            title: "Documentation",
            emoji: "📚",
          },
          style: {
            description: "포매팅, 들여쓰기 등 스타일 변경",
            title: "Styles",
            emoji: "💎",
          },
          refactor: {
            description: "코드 리펙토링",
            title: "Code Refactoring",
            emoji: "📦",
          },
          perf: {
            description: "성능 개선",
            title: "Performance Improvements",
            emoji: "🚀",
          },
          test: {
            description: "테스트 관련 코드 작성 또는 수정",
            title: "Tests",
            emoji: "🚨",
          },
          build: {
            description: "빌드 관련 파일 작성 또는 수정",
            title: "Builds",
            emoji: "🛠",
          },
          ci: {
            description: "CI 설정 파일 작성 또는 수정",
            title: "Continuous Integrations",
            emoji: "⚙️",
          },
          chore: {
            description: "빌드 업무 수정, 패키지 매니저 수정",
            title: "Chores",
            emoji: "♻️",
          },
          revert: {
            description: "이전 코드로 되돌리기",
            title: "Reverts",
            emoji: "🗑",
          },
          release_ops: {
            description: "운영 서버 배포",
            title: "Release:Ops",
            emoji: "🚀",
          },
          release_test: {
            description: "테스트 서버 배포",
            title: "Release:Test",
            emoji: "🚀",
          },
        },
      },
      scope: {
        description:
          "What is the scope of this change (e.g. component or file name)",
      },
      subject: {
        description:
          "Write a short, imperative tense description of the change",
      },
      body: {
        description: "Provide a longer description of the change",
      },
      isBreaking: {
        description: "Are there any breaking changes?",
      },
      breakingBody: {
        description:
          "A BREAKING CHANGE commit requires a body. Please enter a longer description of the commit itself",
      },
      breaking: {
        description: "Describe the breaking changes",
      },

        description: "Does this change affect any open issues?",
      },
      issuesBody: {
        description:
          "If issues are closed, the commit requires a body. Please enter a longer description of the commit itself",
      },
      issues: {
        description: 'Add issue references (e.g. "fix #123", "re #123".)',
      },
    },
  },
};
