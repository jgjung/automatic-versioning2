module.exports = {
  // Type: Conventional Commits에서 사용하는 여러 타입
  // Section: type에 맞는 제목
  // Hidden: log 생성 시 숨김 여부
  types: [
    {
      type: "chore",
      section: "Others",
      hidden: false,
    },
    {
      type: "revert",
      section: "Reverts",
      hidden: false,
    },
    {
      type: "feat",
      section: "Features",
      hidden: false,
    },
    {
      type: "fix",
      section: "Bug Fixes",
      hidden: false,
    },
    {
      type: "improvement",
      section: "Feature Improvements",
      hidden: false,
    },
    {
      type: "inprogress",
      section: "Feature Improvements",
      hidden: false,
    },
    {
      type: "docs",
      section: "Docs",
      hidden: false,
    },
    {
      type: "style",
      section: "Styling",
      hidden: false,
    },
    {
      type: "refactor",
      section: "Code Refactoring",
      hidden: false,
    },
    {
      type: "perf",
      section: "Performance Improvements",
      hidden: false,
    },
    {
      type: "test",
      section: "Tests",
      hidden: false,
    },
    {
      type: "build",
      section: "Build System",
      hidden: false,
    },
    {
      type: "ci",
      section: "CI",
      hidden: false,
    },
  ],
};
