# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.2](https://bitbucket.org/jgjung/automatic-versioning2/compare/v0.0.1...v0.0.2) (2022-01-18)


### Features

* 파이프 라인 1차 마무리 ([093979b](https://bitbucket.org/jgjung/automatic-versioning2/commit/093979b9809f4de5e77377c83a7c3f7ce617676a))

### [0.0.1](https://bitbucket.org/jgjung/automatic-versioning2/compare/v1.1.2...v0.0.1) (2022-01-18)


### Features

* **oh:** ohoh ([42e1760](https://bitbucket.org/jgjung/automatic-versioning2/commit/42e17605f0d57d2bfe9548957b31dfc90c0406f1))

### [1.1.2](https://bitbucket.org/jgjung/automatic-versioning2/compare/v1.1.1...v1.1.2) (2022-01-18)

### [1.1.1](https://bitbucket.org/jgjung/automatic-versioning2/compare/v1.0.0...v1.1.1) (2022-01-18)


### Others

* **release:** 1.1.0 ([3aca052](https://bitbucket.org/jgjung/automatic-versioning2/commit/3aca052a42d4cd4e39e62f093b232a3e4b5497d2))

## [1.1.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v1.0.0...v1.1.0) (2022-01-18)

## [1.0.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v0.0.0...v1.0.0) (2022-01-18)

## [0.0.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v8.2.2...v0.0.0) (2022-01-18)

### [8.2.2](https://bitbucket.org/jgjung/automatic-versioning2/compare/v8.2.1...v8.2.2) (2022-01-18)

### [8.2.1](https://bitbucket.org/jgjung/automatic-versioning2/compare/v8.2.0...v8.2.1) (2022-01-18)

## [8.2.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v8.1.0...v8.2.0) (2022-01-18)

## [8.1.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v8.0.0...v8.1.0) (2022-01-18)

## [8.0.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v7.0.0...v8.0.0) (2022-01-18)

## [7.0.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v6.1.0...v7.0.0) (2022-01-18)


### Features

* **bitbucket pipeline:** 불필요한 단계 제거 ([1c55980](https://bitbucket.org/jgjung/automatic-versioning2/commit/1c5598001bcf4195fa64f31098b9c6a9bf85aa60))


### Bug Fixes

* **bitbucket pipeline:** 파이프 라인 버저닝 작업 중 ([87158d7](https://bitbucket.org/jgjung/automatic-versioning2/commit/87158d7ac23926c7862c6283969ea899467c3bc0))
* **bitbucket pipeline:** 파이프라인 버저닝 브랜치 생성 ([fd12891](https://bitbucket.org/jgjung/automatic-versioning2/commit/fd1289136556cd095829d8bffe1aec532508659f))

## [6.1.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v6.0.0...v6.1.0) (2022-01-18)

## [6.0.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v5.1.0...v6.0.0) (2022-01-18)

## [5.1.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v5.0.2...v5.1.0) (2022-01-18)

### [5.0.2](https://bitbucket.org/jgjung/automatic-versioning2/compare/v5.0.1...v5.0.2) (2022-01-18)

### [5.0.1](https://bitbucket.org/jgjung/automatic-versioning2/compare/v5.0.0...v5.0.1) (2022-01-18)


### Bug Fixes

* **파이프 라인 및 cli 수정:** 수정 ([29f3a18](https://bitbucket.org/jgjung/automatic-versioning2/commit/29f3a18c8527beb11b0c192e444a71fbd1e4bff1))

## [5.0.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v4.1.1...v5.0.0) (2022-01-18)


### Features

* 기능 블라 ([6971839](https://bitbucket.org/jgjung/automatic-versioning2/commit/6971839f7721cef1aa43eb02d356d5802048376f))
* ㄴㅁㅇㄹㄴㅇㄹ ([48d5f68](https://bitbucket.org/jgjung/automatic-versioning2/commit/48d5f681b366adc2609b6cf743553894d54ec8bc))
* 배포 파이프라인 작성 중 ([9b85869](https://bitbucket.org/jgjung/automatic-versioning2/commit/9b85869baf0adb11d2251c8e47173739ecfce8f7))
* **커밋 cli 스크립트 연동:** commitlint, Standard-version과 연동되는 커밋 CLI 스크립트 추가 ([82218b0](https://bitbucket.org/jgjung/automatic-versioning2/commit/82218b02d8ca1a313eb3bdf90aef404069f5a1a9))
* 파이프 라인 작성 ([d96f8a2](https://bitbucket.org/jgjung/automatic-versioning2/commit/d96f8a250c57e22b57335ce4d2de4f6f909216f3))


### Bug Fixes

* **bitbucket-pipelines.yml:** 파이프 라인 스크립트 변경 ([dffdebb](https://bitbucket.org/jgjung/automatic-versioning2/commit/dffdebbfbf213087e031f863ffd13fab9abe4084))
* **파이프 라인 캐싱 추가:** 파이프 라인 캐싱 관련 스크립트 추가 ([7ec8964](https://bitbucket.org/jgjung/automatic-versioning2/commit/7ec89649fa31f596fe766a9d7031dd0ac6b05a07))
* **파이프라인 캐싱 수정:** 캐싱 ([447dab7](https://bitbucket.org/jgjung/automatic-versioning2/commit/447dab7afa648f688240c508dbd6c80c172862d5))
* **파이프라인 캐싱 추가:** node_modules 캐싱 스크립트 추가 ([20cde35](https://bitbucket.org/jgjung/automatic-versioning2/commit/20cde352a1379c4dc53ed47e7cf9a05bb28974fe))

### [4.1.1](https://bitbucket.org/jgjung/automatic-versioning2/compare/v4.1.0...v4.1.1) (2022-01-17)

## [4.1.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v4.0.0...v4.1.0) (2022-01-17)

## [4.0.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v3.0.4...v4.0.0) (2022-01-17)

### [3.0.4](https://bitbucket.org/jgjung/automatic-versioning2/compare/v3.0.3...v3.0.4) (2022-01-17)

### [3.0.3](https://bitbucket.org/jgjung/automatic-versioning2/compare/v3.0.2...v3.0.3) (2022-01-17)

### [3.0.2](https://bitbucket.org/jgjung/automatic-versioning2/compare/v3.0.1...v3.0.2) (2022-01-17)

### [3.0.1](https://bitbucket.org/jgjung/automatic-versioning2/compare/v3.0.0...v3.0.1) (2022-01-17)

## [3.0.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v2.1.3...v3.0.0) (2022-01-17)


### ⚠ BREAKING CHANGES

* 좀 괜찮을지도...?
* It will be significant
* It will be significant

### Bug Fixes

* some message ([a676c0b](https://bitbucket.org/jgjung/automatic-versioning2/commit/a676c0b54a90ae13ac715222fc7eed34e1d6a666))
* some message ([d0c0a34](https://bitbucket.org/jgjung/automatic-versioning2/commit/d0c0a34d27d36249f6cd4baeab2cbf6de57ebc58))


* 문서 변경 ([500adc6](https://bitbucket.org/jgjung/automatic-versioning2/commit/500adc6827555d3d4400f64f9aa9bb7662363fcd))

### [2.1.3](https://bitbucket.org/jgjung/automatic-versioning2/compare/v2.1.2...v2.1.3) (2022-01-17)

### [2.1.2](https://bitbucket.org/jgjung/automatic-versioning2/compare/v2.1.1...v2.1.2) (2022-01-17)

### [2.1.1](https://bitbucket.org/jgjung/automatic-versioning2/compare/v2.1.0...v2.1.1) (2022-01-17)

## [2.1.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v2.0.1...v2.1.0) (2022-01-17)

### [2.0.1](https://bitbucket.org/jgjung/automatic-versioning2/compare/v2.0.0...v2.0.1) (2022-01-17)

## [2.0.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v1.1.0...v2.0.0) (2022-01-17)

## [1.1.0](https://bitbucket.org/jgjung/automatic-versioning2/compare/v1.0.2...v1.1.0) (2022-01-17)

### [1.0.2](https://bitbucket.org/jgjung/automatic-versioning2/compare/v1.0.1...v1.0.2) (2022-01-17)

### 1.0.1 (2022-01-17)
